# Information / Информация

Интеграция дополнительных элементов в движок MediaWiki.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_META`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_META' );
```

## Syntax / Синтаксис

```html

```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
